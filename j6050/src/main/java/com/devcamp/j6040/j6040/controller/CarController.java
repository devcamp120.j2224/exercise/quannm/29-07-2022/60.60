package com.devcamp.j6040.j6040.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j6040.j6040.model.CCar;
import com.devcamp.j6040.j6040.model.CCarType;
import com.devcamp.j6040.j6040.repository.ICarReposiroty;
import com.devcamp.j6040.j6040.repository.ICarTypeReposiroty;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CarController {
    @Autowired
    ICarReposiroty pCarReposiroty;

    @Autowired
    ICarTypeReposiroty pCarTypeReposiroty;

    @GetMapping("/devcamp-cars")
	public ResponseEntity<List<CCar>> getAllCars() {
        try {
            List<CCar> listCars = new ArrayList<CCar>();

            pCarReposiroty.findAll().forEach(listCars::add);

            return new ResponseEntity<List<CCar>>(listCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-cartypes")
	public ResponseEntity<Set<CCarType>> getCarTypeByCarCode(@RequestParam(value = "carCode") String carCode) {
        try {
            CCar vCar = pCarReposiroty.findByCarCode(carCode);
            
            if(vCar != null) {
            	return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
