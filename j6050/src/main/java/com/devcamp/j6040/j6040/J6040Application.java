package com.devcamp.j6040.j6040;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J6040Application {

	public static void main(String[] args) {
		SpringApplication.run(J6040Application.class, args);
	}

}
